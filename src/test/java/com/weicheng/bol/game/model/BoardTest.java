package com.weicheng.bol.game.model;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

public class BoardTest {

	@Test
	public void testPickStone_success_turnEnd() {
		Board board = new Board();
		board.pickStone(0, 3);
		assertEquals(Player.PLAYER_2, board.getCurrentPlayer());
	}
	
	@Test
	public void testPickStone_success_oneMoreTurn() {
		Board board = new Board();
		board.pickStone(0, 6);
		assertEquals(Player.PLAYER_1, board.getCurrentPlayer());
	}
	
	@Test
	public void testPickStone_success_playerTwo() {
		Board board = new Board();
		board.pickStone(1, 5);
	}
	
	@Test
	public void testPickStone_captureStone() {
		Board board = new Board(Arrays.asList(0,0,1,6,6,6,6,6,6,6,6,6,6,0),null,null);
		board.pickStone(0, 2);
		assertEquals(Arrays.asList(7,0,0,6,6,6,6,0,6,6,6,6,6,0),board.getPits());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPickStone_emptyPit() {
		Board board = new Board(Arrays.asList(0,6,0,6,6,6,6,0,6,6,6,6,6,6,0),null,null);
		board.pickStone(0, 2);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPickStone_pickFromBigPit_Player1() {
		Board board = new Board();
		board.pickStone(0, 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPickStone_pickFromBigPit_Player2() {
		Board board = new Board();
		board.pickStone(1, 6);
	}

	@Test
	public void testPickStone_triggerGameEnd_playerOneWin() {
		Board board = new Board(Arrays.asList(10,0,0,1,0,0,0,0,0,0,0,0,2,5),null,null);
		board.pickStone(0, 3);
		assertEquals(GameEndState.PLAYER_ONE_WIN, board.getGameEndState());
	}
	
	@Test
	public void testPickStone_triggerGameEnd_playerTwoWin() {
		Board board = new Board(Arrays.asList(3,0,0,1,0,0,0,0,0,0,0,0,1,5),null,null);
		board.pickStone(0, 3);
		assertEquals(GameEndState.PLAYER_TWO_WIN, board.getGameEndState());
	}
	
	@Test
	public void testPickStone_triggerGameEnd_draw() {
		Board board = new Board(Arrays.asList(3,0,0,1,0,0,0,0,0,0,0,0,1,3),null,null);
		board.pickStone(0, 3);
		assertEquals(GameEndState.DRAW, board.getGameEndState());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPickStone_gameAlreadyEnded() {
		Board board = new Board();
		ReflectionTestUtils.setField(board, "gameEndState", GameEndState.DRAW);
		board.pickStone(0, 1);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPickStone_playOutOfTurn() {
		Board board = new Board();
		ReflectionTestUtils.setField(board, "currentPlayer", Player.PLAYER_2);
		board.pickStone(0, 1);
		
	}
}
