package com.weicheng.bol.game.controller;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.weicheng.bol.game.model.Board;

@RestController
@Validated
@Scope("request")
public class BoardController {
	@Autowired
	private Board board;
	
	@GetMapping("/board")
    public Board board() {
        return board;
    }
	
	@PutMapping("/board")
	public Board pickStone(@Range(min = 0, max = 1) @RequestParam(value = "row", required = true) int row,
			@Range(min = 0, max = 6) @RequestParam(value = "column", required = true) int column) {
		board.pickStone(row, column);
		return board;
	}
	
	@DeleteMapping("/board")
	public Board deleteBoard() {
		board.reset();
		return board;
	}
}
