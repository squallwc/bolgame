package com.weicheng.bol.game.model;

public enum GameEndState {
	DRAW,
	PLAYER_ONE_WIN,
	PLAYER_TWO_WIN,
}
