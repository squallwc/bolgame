package com.weicheng.bol.game.model;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * The board that store state and handle logic for the game.
 * This game is also known as Kalaha, see https://www.youtube.com/watch?v=7x86xyYd3_A
 *
 */
@Component
@Scope("session")
public class Board {
	//6 stones in each pits, big pit at the beginning and end of the array
	private static final List<Integer> PIT_INITIAL_LAYOUT = Arrays.asList(0,6,6,6,6,6,6,6,6,6,6,6,6,0);
	
	private PitCollection pitCollection;
	private Player currentPlayer; //null if the game is not started yet, either player can start the turn
	private GameEndState gameEndState; //null if the game is not started yet or still on-going

	// initial board setup
	public Board() {
		this.pitCollection = new PitCollection(PIT_INITIAL_LAYOUT);
	}
	
	public Board(List<Integer> pits, Player currentPlayer, GameEndState gameEndState) {
		this.pitCollection = new PitCollection(pits);
		this.currentPlayer = currentPlayer;
		this.gameEndState = gameEndState;
	}

	/**
	 * pick all the stone from the specific pit and sow stone to the right
	 * 
	 * @param row
	 * @param column
	 */
	public void pickStone(int row, int column) {
		if(gameEndState!=null) {
			throw new IllegalArgumentException("game already ended");
		}
		if((row==0 && column==0) || (row==1 && column==6)) {
			throw new IllegalArgumentException("cannot pick from big pit");
		}
		
		int index = row*7+column;
		
		if(row==0) {
			if(currentPlayer==Player.PLAYER_2) {
				throw new IllegalArgumentException("acting out of turn");
			}
			currentPlayer=Player.PLAYER_1;
		}else if(row==1) {
			// acting out of turn
			if(currentPlayer==Player.PLAYER_1) {
				throw new IllegalArgumentException("acting out of turn");
			}
			currentPlayer=Player.PLAYER_2;
		}
		
		Pit lastPit = pitCollection.sowStone(index,currentPlayer);
		//determine which player acts next turn
		if(currentPlayer==Player.PLAYER_1) {
			if(lastPit.isPlayer1BigPit()) {
				currentPlayer=Player.PLAYER_1;
			}else {
				currentPlayer=Player.PLAYER_2;
			}
		}
		else if(currentPlayer==Player.PLAYER_2) {
			if(lastPit.isPlayer2BigPit()) {
				currentPlayer=Player.PLAYER_2;
			}else {
				currentPlayer=Player.PLAYER_1;
			}
		}
		
		//check end game condition
		if(isEitherSidePitsEmpty()) {
			pitCollection.addAllStonesToBigPit();
			
			int player1Score = pitCollection.get(Pit.PLAYER_1_BIG_PIT_INDEX);
			int player2Score = pitCollection.get(Pit.PLAYER_2_BIG_PIT_INDEX);
			
			if(player1Score>player2Score) {
				gameEndState=GameEndState.PLAYER_ONE_WIN;
			}else if(player1Score<player2Score){
				gameEndState=GameEndState.PLAYER_TWO_WIN;
			}else {
				gameEndState=GameEndState.DRAW;
			}
		}
	}
	
	public void reset() {
		this.pitCollection = new PitCollection(PIT_INITIAL_LAYOUT);
		this.currentPlayer=null;
		this.gameEndState=null;
	}

	private boolean isEitherSidePitsEmpty() {
		List<Integer> pits = pitCollection.toList();
		
		boolean playerOneEmpty = true;
		for(int i=1;i<7;i++) {
			if(pits.get(i)>0) {
				playerOneEmpty=false;
				break;
			}
		}
		
		boolean playerTwoEmpty = true;
		for(int i=7;i<pits.size()-2;i++) {
			if(pits.get(i)>0) {
				playerTwoEmpty=false;
				break;
			}
		}
		return playerOneEmpty || playerTwoEmpty;
	}
	
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public List<Integer> getPits() {
		return pitCollection.toList();
	}

	public GameEndState getGameEndState() {
		return gameEndState;
	}
}
