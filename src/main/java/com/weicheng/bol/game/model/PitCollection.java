package com.weicheng.bol.game.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Circular linked list that traverse counter-clockwise. Handle the sow stone logic. 
 * Search node with index in O(1), backed by HashMap.
 * 
 */
public class PitCollection {
	private final Map<Integer,Pit> map = new HashMap<>();
	
	public PitCollection(List<Integer> pits) {
		Pit head = new Pit(0, pits.get(0));
		map.put(0, head);
		Pit current = head;
		
		for(int i=7;i<pits.size();i++) {
			Pit eachPit = new Pit(i, pits.get(i));
			map.put(i, eachPit);
			current.setNext(eachPit);
			current = eachPit;
		}
		for(int i=6;i>=1;i--) {
			Pit eachPit = new Pit(i, pits.get(i));
			map.put(i, eachPit);
			current.setNext(eachPit);
			current = eachPit;
		}
		
		current.setNext(head);
	}
	
	/**
	 * Sow stone counter-clockwise
	 * @param index
	 * @param player
	 * @return the last element that has been sowed
	 */
	public Pit sowStone(int index, Player player) {
		Pit current = map.get(index);
		int stoneAmount = current.pickAllStone();
		if(stoneAmount==0) {
			throw new IllegalArgumentException("cannot pick from empty pit");
		}
		
		while(stoneAmount>0) {
			current = current.getNext();
			
			//skip opponent's big pit
			if(player==Player.PLAYER_1 && current.isPlayer2BigPit()) continue;
			if(player==Player.PLAYER_2 && current.isPlayer1BigPit()) continue;
			
			current.addStone(1);
			stoneAmount--;
		}
		
		//if last pit was empty, add all stones to big pit and capture opposite stones
		if(current.getValue()==1) {
			if(player==Player.PLAYER_1 && !current.isPlayer1BigPit() && current.getIndex()<=6) {
				Pit oppositePit = map.get(current.getIndex()+6);
				Pit bigPit = map.get(Pit.PLAYER_1_BIG_PIT_INDEX);
				bigPit.addStone(current.pickAllStone() + oppositePit.pickAllStone());
			}else if(player==Player.PLAYER_2 && !current.isPlayer2BigPit() && current.getIndex()>6) {
				Pit oppositePit = map.get(current.getIndex()-6);
				Pit bigPit = map.get(Pit.PLAYER_2_BIG_PIT_INDEX);
				bigPit.addStone(current.pickAllStone() + oppositePit.pickAllStone());
			}
		}
		return current;
	}
	
	/**
	 * Add all stones in first row to player 1's big pit and stones in second row to player 2's big pit
	 */
	public void addAllStonesToBigPit() {
		//player 1
		int sum = 0;
		for(int i=1;i<7;i++) {
			sum += map.get(i).pickAllStone();
		}
		map.get(Pit.PLAYER_1_BIG_PIT_INDEX).addStone(sum);
		
		//player 2
		sum = 0;
		for(int i=7;i<13;i++) {
			sum += map.get(i).pickAllStone();
		}
		map.get(Pit.PLAYER_2_BIG_PIT_INDEX).addStone(sum);
	}
	
	public int get(int index) {
		return map.get(index).getValue();
	}
	
	public List<Integer> toList(){
		List<Integer> list = new ArrayList<>();
		for(int i=0;i<14;i++) {
			list.add(map.get(i).getValue());
		}
		return list;
	}
}
