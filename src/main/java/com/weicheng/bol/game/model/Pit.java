package com.weicheng.bol.game.model;

public class Pit {
	public static final int PLAYER_1_BIG_PIT_INDEX = 0;
	public static final int PLAYER_2_BIG_PIT_INDEX = 13;
	
	private int value;
	private final int index;
	private Pit next;
	
	public Pit(int index, int value) {
		super();
		this.value = value;
		this.index = index;
	}
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int pickAllStone() {
		int temp = value;
		this.value = 0;
		return temp;
	}
	public void addStone(int value) {
		this.value += value;
	}

	public boolean isPlayer1BigPit() {
		return index==PLAYER_1_BIG_PIT_INDEX;
	}

	public boolean isPlayer2BigPit() {
		return index==PLAYER_2_BIG_PIT_INDEX;
	}

	public boolean isEmpty() {
		return value<=0;
	}

	@Override
	public String toString() {
		return ""+value;
	}

	public Pit getNext() {
		return next;
	}

	public void setNext(Pit next) {
		this.next = next;
	}

	public int getIndex() {
		return index;
	}
}
