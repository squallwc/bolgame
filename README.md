# Bol.com game
## Command to run the web application
`$ mvn spring-boot:run`

or with the included maven wrapper
`$ mvnw spring-boot:run`

## Open with browser
http://localhost:8080

## REST API
http://localhost:8080/board

* GET - game state in json
* PUT - pick stone in specified pit
  - Parameters
     - row(mandatory): 0 for player1, 1 for player2
     - column(mandatory): index of pit from 0-6

* DELETE - reset the game to initial state